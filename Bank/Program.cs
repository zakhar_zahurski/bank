﻿class MainClass
{
    public static void Main(string[] args)
    {        
        bool cycle = true;
        while (cycle)
        {
            Console.Clear();
            Console.WriteLine($"{Bank.bankName} \n{Bank.address}");
            Console.WriteLine("====================\n\n1.Добавить клиента\n2.Удалить клиента\n3.Изменить имя/фамилию клиента\n4.Изменить баланс клиента\n5.Информация о клиенте\n6.Информация о всех клиентах\n7.Статистика банка\n8.Выход\n");
            int num = Convert.ToInt32(Console.ReadLine());

            switch (num)
            {
                case 1:
                    Bank.Add();
                    break;
                case 2:
                    Bank.Remove();
                    break;
                case 3:
                    Client.ChangeClientName();
                    break;
                case 4:
                    Client.ChangeClientBalance();
                    break;
                case 5:
                    Bank.ShowClient();
                    break;
                case 6:
                    Bank.ShowAllClient();
                    break;
                case 7:
                    Bank.Stats();
                    break;
                case 8:
                    cycle = false;
                    break;
            }
        }
    }
}
class Bank
{
    public static string bankName = "====================\nTinkoff";
    public static string address = "Москва, 2-я Хуторская ул., 38А строение 26";
    public static List<Client> clientList = new List<Client>();

    public static void Add()
    {
        Console.Clear();
        Console.WriteLine("1.Введите имя клиента");
        string name = Console.ReadLine();
        Console.WriteLine("2.Введите фамилию клиента");
        string surname = Console.ReadLine();
        Console.WriteLine("3.Введите возраст клиента");
        int age = Convert.ToInt32(Console.ReadLine());
        Console.Clear();
        Console.WriteLine($"Клиент {surname} {name} добавлен!");
        Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
        Console.ReadLine();


        int balance = 0;
        Client client = new Client(name, surname, age, balance);

        clientList.Add(client);
    }

    public static void Remove()
    {
        Console.Clear();
        Console.WriteLine("Введите имя клиента которого нужно удалить");
        string name = Console.ReadLine();
        for(int i = 0; i < clientList.Count; i++)
        {
            if (name == clientList[i].Name)
            {
                string clientName = Bank.clientList[i].Name;
                string clientSurname = Bank.clientList[i].Surname;
                clientList.Remove(Bank.clientList[i]);
                Console.WriteLine($"Клиент {clientSurname} {clientName} удалён!");
                Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
                Console.ReadLine();
            }
        }
    }
    public static void Stats()
    {
        Console.Clear();
        if (clientList.Count == 0)
        {
            Console.WriteLine($"В банке нет клиентов!");
        }
        else
        {
            int sumMoney = 0;
            int rich = clientList[0].Balance;
            string nameRich = clientList[0].Name;
            string surnameRich = clientList[0].Surname;
            Console.WriteLine($"Всего клиентов в банке {clientList.Count}");
            for (int i = 0; i < clientList.Count; i++)
            {
                sumMoney += clientList[i].Balance;
            }
            Console.WriteLine($"Всего денег на счетах в банке {sumMoney}");
            for (int i = 1; i < clientList.Count; i++)
            {
                if (rich < clientList[i].Balance)
                {
                    rich = clientList[i].Balance;
                    nameRich = clientList[i].Name;
                    surnameRich = clientList[i].Surname;
                }
            }
            Console.WriteLine($"Самый богатый клиент в банке {surnameRich} {nameRich} имеет {rich}");
        }
        Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
        Console.ReadLine();
    }

    public static void ShowClient()
    {
        Console.Clear();
        Console.WriteLine("Введите имя клиента для получения информации");
        string name = Console.ReadLine();
        for (int i = 0; i < Bank.clientList.Count; i++)
        {
            if (name == Bank.clientList[i].Name)
            {
                Console.WriteLine($"Фамилия: {clientList[i].Surname}\nИмя: {clientList[i].Name}\nВозраст: {clientList[i].Age}\nБаланс: {clientList[i].Balance}");                
            }
        }
        Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
        Console.ReadLine();
    }

    public static void ShowAllClient()
    {
        Console.Clear();
        if (clientList.Count == 0)
        {
            Console.WriteLine($"В банке нет клиентов!");
        }
        else
        {
            Console.WriteLine($"Всего клиентов в банке {clientList.Count}\n====================\n");
            clientList.Sort((left, right) => left.Surname.CompareTo(right.Surname));
            foreach (Client sortedClientList in clientList)
            {
                Console.WriteLine($"Фамилия: {sortedClientList.Surname}\nИмя: {sortedClientList.Name}\nВозраст: {sortedClientList.Age}\nБаланс: {sortedClientList.Balance}\n====================\n");
            }            
        }
        Console.WriteLine("Для продолжения нажмите любую кнопку");
        Console.ReadLine();
    }
}
class Client
{
    public string Name;
    public string Surname;
    public int Age;
    public int Balance;

    public Client()
    {
        Name = "name";
        Surname = "surname";
        Age = 0;
        Balance = 0;
    }
    public Client(string name, string surname, int age, int balance) 
    {
        Name = name;
        Surname = surname;
        Age = age;
        Balance = balance;
    }

    public static void ChangeClientName()
    {
        Console.Clear();
        Console.WriteLine("Введите имя клиента чтобы изменить имя/фамилию");
        string name = Console.ReadLine();
        for (int i = 0; i < Bank.clientList.Count; i++)
        {
            if (name == Bank.clientList[i].Name)
            {
                Console.WriteLine("1.Введите новое имя");
                string newName = Console.ReadLine();
                Bank.clientList[i].Name = newName;
                Console.WriteLine("2.Введите новую фамилию");
                string newSurname = Console.ReadLine();
                Bank.clientList[i].Surname = newSurname;
                Console.WriteLine("Имя и фамилия изменены!");
                Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
                Console.ReadLine();
            }
        }
    }

    public static void ChangeClientBalance()
    {
        Console.Clear();
        Console.WriteLine("Введите имя клиента для изменения баланса\n");
        string name = Console.ReadLine();
        for (int i = 0; i < Bank.clientList.Count; i++)
        {
            if (name == Bank.clientList[i].Name)
            {
                Console.WriteLine("1.Увеличить баланс\n2.Уменьшить баланс\n");
                int key = Convert.ToInt32(Console.ReadLine());                
                switch (key)
                {
                    case 1:
                        Console.WriteLine("Введите сумму начисления\n");
                        int money = Convert.ToInt32(Console.ReadLine());
                        Bank.clientList[i].Balance += money;
                        Console.WriteLine($"Баланс клиента {Bank.clientList[i].Surname} {Bank.clientList[i].Name} увеличен на {money}\n");
                        Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
                        Console.ReadLine();
                        break;
                    case 2:
                        Console.WriteLine("Введите сумму списания\n");
                        money = Convert.ToInt32(Console.ReadLine());
                        Bank.clientList[i].Balance -= money;
                        Console.WriteLine($"Баланс клиента {Bank.clientList[i].Surname} {Bank.clientList[i].Name} уменьшен на {money}\n");
                        Console.WriteLine("\n====================\nДля продолжения нажмите любую кнопку");
                        Console.ReadLine();
                        break;
                }                
            }
        }

    }    
}



    
        


